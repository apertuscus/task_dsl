local gr_spec =require "gr_spec"
--local tostring 



point = gr_spec.Point{x=1,y=2,z=3}
umf.check(point,gr_spec.EntitySpec{},true)
versor = gr_spec.Point{x=0,y=0,z=1}
line = gr_spec.Line{origin=gr_spec.Point{x=0,y=0,z=0},direction=gr_spec.Versor{x=1,y=0,z=0}}
plane = gr_spec.Plane{origin=gr_spec.Point{x=0,y=0,z=0},normal=gr_spec.Versor{x=1,y=0,z=0}}


o1=gr_spec.ObjectFrame{frame_name="o1"}
o2=gr_spec.ObjectFrame{frame_name="o2"}
umf.check(o1,gr_spec.object_frame_spec,true)
pr_line = gr_spec.Primitive{entity=line,object_frame=o1}
pr_point = gr_spec.Primitive{entity=point,object_frame=o1}
pr_plane = gr_spec.Primitive{entity=plane,object_frame=o2}
pr_versor = gr_spec.Primitive{entity=versor,object_frame=o2}
umf.check(pr_line,gr_spec.primitive_spec,true)

g1 = gr_spec.GeometricExpression{p1=pr_point,p2=pr_point,expression=	"point-point distance"}
g2 = gr_spec.GeometricExpression{p1=pr_point,p2=pr_line,expression=	"line-point distance"}
g3 = gr_spec.GeometricExpression{p1=pr_line,p2=pr_line,expression=	"line-point distance"}
g4 = gr_spec.GeometricExpression{p1=pr_point,p2=pr_point,expression=	"line-point distance"}
g5 = gr_spec.GeometricExpression{p1=pr_line,p2=pr_line,expression=	"distance from lines"}
g6 = gr_spec.GeometricExpression{p1=pr_plane,p2=pr_line,expression=	"distance from lines"}




j1 = gr_spec.JointExpression{joint_names={"j1"},expression="single_joint_value"}




print("======\n output_expression..\n==========")

umf.check(g1,gr_spec.ExpressionSpec{},true)
print("==========\n constraints..\n==========")
print("==========\n1: ok\n==========")
umf.check(gr_spec.Constraint{output_expression=g2,behaviour="Velocity Limit"},gr_spec.constraint_spec,true)
print("==========\n2: not ok\n==========")
umf.check(gr_spec.Constraint{output_expression=g3,behaviour="Velocity Limit"},gr_spec.constraint_spec,true)
print("==========\n3: not ok\n==========")
umf.check(gr_spec.Constraint{output_expression=g4,behaviour="Velocity Limit"},gr_spec.constraint_spec,true)
print("==========\n4: ok\n==========")
umf.check(gr_spec.Constraint{output_expression=g5,behaviour="Velocity Limit"},gr_spec.constraint_spec,true)
print("==========\n5: not ok\n==========")
umf.check(gr_spec.Constraint{output_expression=g6,behaviour="Velocity Limit"},gr_spec.constraint_spec,true)
print("==========\n6: ok\n==========")
umf.check(gr_spec.Constraint{output_expression=j1,behaviour="Velocity Limit"},gr_spec.constraint_spec,true)
print("==========\n7: ok\n==========")
c1=gr_spec.Constraint{output_expression=g1,behaviour="Velocity Limit"}
umf.check(c1,gr_spec.constraint_spec,true)
print("==========\n8: not ok\n==========")
c2=gr_spec.Constraint{output_expression=o1,behaviour="Velocity Limit"}
umf.check(c2,gr_spec.constraint_spec,true)
print("==========\n9: ok\n==========")

M1=gr_spec.GJMonitor{monitor_expression=g1,
	event_risen="done",
	monitored_variable_type="POS",
	comparison_type="IN_INTERVAL",
	lower_bound=2,
	upper_bound=3,
}




M2=gr_spec.ExtMonitor{
	event_risen="done",
	monitored_variable_name="gripper_force",
	comparison_type="LESS",
	upper_bound=2,
}
print("==========\n Monitors..\n==========")
umf.check(M1,gr_spec.MonitorSpec{},true)
umf.check(M2,gr_spec.MonitorSpec{},true)
print("==========\n tasks..\n==========")
prim_c={c1}
--i can pass empty table and pass the check, if precheck is not implemented
T1=gr_spec.Task{primary_constraints=prim_c,monitors={M1}}
umf.check(T1,gr_spec.task_spec,true)
T2=gr_spec.Task{primary_constraints=prim_c,monitors={M2}}
umf.check(T2,gr_spec.task_spec,true)


